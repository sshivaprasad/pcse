#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
using namespace std;
#include <mpi.h>
#include <math.h>

int main(int argc,char **argv) {
  int mytid, procs;
  MPI_Comm comm;

  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_rank(comm,&mytid);
MPI_Comm_size(MPI_COMM_WORLD, &procs);

  {
    double tstart,tstop,jitter,avg,d;
    void *sendbuf;
    int wait,i=0;
        double *recv;
    // wait for a random time, and measure how long
    wait = (int) ( 6.*rand() / (double)RAND_MAX );
    tstart = MPI_Wtime();
    sleep(wait);
    tstop = MPI_Wtime();
    jitter = tstop-tstart-wait;


// find the maximum time over all processors
     if (mytid==0)
        {
                sendbuf = (void*)&jitter;//MPI_IN_PLACE;
                recv = (double*) malloc(sizeof sendbuf );
        }
     else
        {
        sendbuf = (void*)&jitter;
        }
        printf("jitter  = %e, id = %d\n",jitter,mytid);
        MPI_Gather(sendbuf, 1, MPI_DOUBLE, recv, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        if (mytid==0)
        {
                for(i=0; i < procs; i++)
                printf("gather value: %e, id = %d\n",recv[i],i);
        }


}

  MPI_Finalize();
  return 0;
}
