#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>
using namespace std;
#include <mpi.h>
#include <math.h>

int main(int argc,char **argv) {
  int mytid, procs;
  MPI_Comm comm;

  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_rank(comm,&mytid);
MPI_Comm_size(MPI_COMM_WORLD, &procs);

  {
    double tstart,tstop,jitter,avg,d;
    void *sendbuf;
    int wait;

    // wait for a random time, and measure how long
    wait = (int) ( 6.*rand() / (double)RAND_MAX );
    tstart = MPI_Wtime();
    sleep(wait);
    tstop = MPI_Wtime();
    jitter = tstop-tstart-wait;
// find the maximum time over all processors
     if (mytid==0)
        sendbuf = MPI_IN_PLACE;
     else sendbuf = (void*)&jitter;
     MPI_Allreduce(sendbuf,(void*)&avg,1,MPI_DOUBLE,MPI_SUM,comm);
     avg = avg/procs;
        if(mytid == 0)  printf("average = %e\n", avg);
        avg = jitter - avg;
        avg = avg * avg;
        MPI_Reduce((void*)&avg, (void*)&d,1,MPI_DOUBLE,MPI_SUM,0,comm);
        
        if (mytid==0)
        {
                d = d / procs;
                d = sqrt(d);
                printf("std dev jitter: %e\n",d);        
        }
  }

  MPI_Finalize();
  return 0;
}

